Source: barry
Section: misc
Priority: optional
Maintainer: Chris Frey <cdfrey@foursquare.net>
Build-Depends: debhelper (>= 4.0.0), cdbs, autoconf, automake, libtool, pkg-config, libusb-dev, libboost-serialization-dev, libtar-dev, libgtkmm-2.4-dev, libglademm-2.4-dev, libopensync0-dev (>= 0.22), libopensync0-dev (<< 0.30)
Standards-Version: 3.6.1

Package: libbarry
Section: libs
Architecture: any
Depends: ${shlibs:Depends}
Description: Library for using the BlackBerry handheld on Linux
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains shared libraries.

Package: libbarry-dbg
Section: libs
Priority: extra
Architecture: any
Depends: libbarry
Description: Library for using the BlackBerry handheld on Linux
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains the debug version of the libbary shared library.

Package: libbarry-dev
Section: libdevel
Architecture: any
Depends: libbarry
Description: Development files for libbarry
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains the header files required for building applications
 based on libbarry.

Package: barry-util
Section: utils
Architecture: any
Depends: udev (>= 0.056), ${shlibs:Depends}
Suggests: ppp
Description: Command line utilities for working with the RIM BlackBerry Handheld
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains command line utilities, such as bcharge, btool,
 breset, etc.

Package: barry-util-dbg
Section: utils
Priority: extra
Architecture: any
Depends: barry-util
Description: Command line utilities for working with the RIM BlackBerry Handheld
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains the debug versions of the command line utilities.

Package: barrybackup-gui
Section: utils
Architecture: any
Depends: ${shlibs:Depends}
Description: GTK+ based GUI for backing up the RIM BlackBerry Handheld
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains a GUI application for making backups and restores.

Package: barrybackup-gui-dbg
Section: utils
Priority: extra
Architecture: any
Depends: barrybackup-gui
Description: GTK+ based GUI for backing up the RIM BlackBerry Handheld
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains the debug version of the backup GUI.

Package: libopensync-plugin-barry
Section: libs
Architecture: any
Depends: libopensync0 (>= 0.22), libopensync0 (<< 0.30), ${shlibs:Depends}
Description: Opensync Blackberry plugin, based on the Barry project
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains an opensync plugin for use with libopensync 0.22.

Package: libopensync-plugin-barry-dbg
Section: libs
Priority: extra
Architecture: any
Depends: libopensync0 (>= 0.22), libopensync0 (<< 0.30), libopensync-plugin-barry
Description: Opensync Blackberry plugin, based on the Barry project
 Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
 .
 This package contains the debug version of the Barry opensync plugin.

#Package: barry-doc
#Architecture: all
#Description: Documentation for barry
# Barry is a GPL C++ library for interfacing with the RIM BlackBerry Handheld.
# It comes with a command line tool for exploring the device and for making quick
# backups. The goal of this project is to create a fully functional syncing
# mechanism on Linux.

